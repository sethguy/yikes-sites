---
to: app/routes/<%= name %>Route.js
---
const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const <%= name %> = require('../NodeServices/<%= name %>Service.js')

router.post('/', (req, res) => {

  console.log('req.body @ <%= name %> post route', req.body)

  <%= name %>

    .post(req.body)

    .subscribe((post<%= h.capitalize(name) %>Response) => {

      res.send({
        post<%= h.capitalize(name) %>Response
      })

    })

}); //POST 

router.put('/', (req, res) => {

  console.log('req.body @ <%= name %> put route', req.body)

  <%= name %>

    .update(req.body)

    .subscribe((put<%= h.capitalize(name) %>Response) => {

      res.send({
        put<%= h.capitalize(name) %>Response
      })

    })

}) //PUT

router.get('/', (req, res) => {

  return <%= name %>

    .get(req.query)

    .subscribe((get<%= h.capitalize(name) %>Response) => {

      res.send(get<%= h.capitalize(name) %>Response)

    })

})

router.delete('/', (req, res) => {

  return <%= name %>

    .delete(req.query)

    .subscribe((get<%= h.capitalize(name) %>Response) => {

      res.send(get<%= h.capitalize(name) %>Response)

    })

})


module.exports = router;