---
to: app/routes/index.js
---
const express = require('express');

const router = express.Router();

var routes = [
	{name:'health'},
	{name:'images'},
	{name:'users'}
]

routes.forEach( ( routeConfig ) => {

	var routePath = `/${routeConfig.name}`;

	var routeSrcPath = `./${routeConfig.name}Route`;

	router.use(routePath, require(routeSrcPath) )

})

// GET home page
router.get('/', (req, res) => {

  res.set('Content-Type', 'text/html');

  res.send('<p>noder</p>');

//res.send(fs.readFileSync('./View/build/index.html'));

});

router.post('/', (req, res) => {

    console.log(req.body)
    res.send(req.body);

});

module.exports = router;