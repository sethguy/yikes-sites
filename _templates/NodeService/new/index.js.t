---
to: app/NodeServices/<%= name %>Service.js
---
const db = require('./dbService');
const Rx = require('rxjs');

const _ = require('lodash');

const <%= name %>CollectionName = '<%= name %>'

const list = [
  {
    keyName: 'tags',
    props: ['text']
  }
]

var post = function(<%= name %>) {

  return db

    .post(<%= name %>CollectionName, <%= name %>)

}

var put = function(<%= name %>) {

  return db

    .update(<%= name %>CollectionName, <%= name %>, {
      _id: <%= name %>._id
    })

}

var update = function(<%= name %>) {

  var query = {
    _id: <%= name %>._id
  }

  return db.get(<%= name %>CollectionName, query)

    .switchMap((getResponse) => {

      list.forEach(listItem => {

        if (<%= name %>[listItem.keyName]) {

          getResponse[0][listItem.keyName] = <%= name %>[listItem.keyName]
        }

      })

      var <%= name %>Update = _.merge(getResponse[0], <%= name %>)

      return db

        .update(<%= name %>CollectionName, <%= name %>Update, {
          _id: <%= name %>._id
        })

    })

}


var remove = function(query) {

  return db.delete(<%= name %>CollectionName, query)

}

var get = function(query) {

  if (query.searchTerm) {

    query = {
      'name': {
        $regex: ".*" + query.searchTerm + ".*",
        $options: "i"
      }
    }

  }

  return db.get(<%= name %>CollectionName, query)

}

var <%= name %>Service = {

  delete: remove,

  get,

  post,

  update,

  put
}

module.exports = <%= name %>Service