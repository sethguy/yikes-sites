---
to: app/docker-compose
---
version: '2'

services:

  node:
    build:
      context: .
      dockerfile: node.dockerfile
    ports:
      - "80:8080"
      - "443:443"

    networks:
      - nodeapp-network

  mongodb:
    image: mongo
    networks:
      - nodeapp-network

  mongo-express:
    build:
      context: .
      dockerfile: mongo-express.dockerfile  
    ports:
      - "8081:8081"
    networks:
      - nodeapp-network

networks:
  nodeapp-network:
    driver: bridge