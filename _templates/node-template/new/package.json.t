---
to: app/package.json
---
{
  "name": "<%= appName %>",
  "version": "1.0.0",
  "description": "a new app",
  "main": "index.js",
  "scripts": {
    "start": "node server.js",
    "test": "NODE_ENV=test PORT=3333 mocha --recursive --reporter spec"
  },
  "author": "seth",
  "license": "",
  "devDependencies": {
    "chai": "^3.5.0",
    "mocha": "^3.2.0",
    "nodemon": "^1.11.0",
    "supertest": "^2.0.1"
  },
  "dependencies": {
    "async": "^2.4.0",
    "body-parser": "^1.17.2",
    "express": "^4.14.0",
    "forever": "^0.15.3",
    "formidable": "^1.1.1",
    "gm": "^1.23.0",
    "gridfs-stream": "^1.1.1",
    "imagemagick": "^0.1.3",
    "install": "^0.10.1",
    "lodash": "^4.17.5",
    "mongodb": "^2.2.25",
    "nodemailer": "^4.0.1",
    "npm": "^4.6.1",
    "request": "^2.81.0",
    "rx-mongodb": "^1.0.0",
    "rxjs": "^5.4.0"
  },
  "engines": {
    "node": ">= 6.8.1"
  }
}
