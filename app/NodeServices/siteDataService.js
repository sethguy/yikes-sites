const db = require('./dbService');
const Rx = require('rxjs');

const _ = require('lodash');

const siteDataCollectionName = 'siteData'

const list = [
  {
    keyName: 'tags',
    props: ['text']
  }
]

var post = function(siteData) {

  return db

    .post(siteDataCollectionName, siteData)

}

var put = function(siteData) {

  return db

    .update(siteDataCollectionName, siteData, {
      _id: siteData._id
    })

}

var update = function(siteData) {

  var query = {
    _id: siteData._id
  }

  return db.get(siteDataCollectionName, query)

    .switchMap((getResponse) => {

      var siteDataUpdate = _.extend(getResponse[0], siteData)

      return db

        .update(siteDataCollectionName, siteDataUpdate, {
          _id: siteData._id
        })

    })

}

var remove = function(query) {

  return db.delete(siteDataCollectionName, query)

}

var get = function(query) {

  if (query.searchTerm) {

    query = {
      'name': {
        $regex: ".*" + query.searchTerm + ".*",
        $options: "i"
      }
    }

  }

  return db.get(siteDataCollectionName, query)

}

var siteDataService = {

  delete: remove,

  get,

  post,

  update,

  put
}

module.exports = siteDataService