const db = require('./dbService');
const Rx = require('rxjs');

const _ = require('lodash');

const sitePagesCollectionName = 'sitePages'

const list = [
  {
    keyName: 'tags',
    props: ['text']
  }
]

var post = function(sitePages) {

  return db

    .post(sitePagesCollectionName, sitePages)

}

var put = function(sitePages) {

  return db

    .update(sitePagesCollectionName, sitePages, {
      _id: sitePages._id
    })

}

var update = function(sitePages) {

  var query = {
    _id: sitePages._id
  }

  return db.get(sitePagesCollectionName, query)

    .switchMap((getResponse) => {

      var sitePagesUpdate = _.extend(getResponse[0], sitePages)

      return db

        .update(sitePagesCollectionName, sitePagesUpdate, {
          _id: sitePages._id
        })

    })

}


var remove = function(query) {

  return db.delete(sitePagesCollectionName, query)

}

var get = function(query) {

  if (query.searchTerm) {

    query = {
      'name': {
        $regex: ".*" + query.searchTerm + ".*",
        $options: "i"
      }
    }

  }

  return db.get(sitePagesCollectionName, query)

}

var sitePagesService = {

  delete: remove,

  get,

  post,

  update,

  put
}

module.exports = sitePagesService