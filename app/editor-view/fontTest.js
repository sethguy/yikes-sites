var fontData = require('./fontslist')

var getGoogleFontPage = function(top, skip) {

  return fontData.items

    .filter((font, i) => {

      return (i >= skip) && (i < (skip + top))

    })

}