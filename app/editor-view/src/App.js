import React, { Component } from 'react';

//SYLE IMPORTS
import './App.css';


//CUSTOM IMPORTS
import { saveState, streamStart } from './Services/ContentState'



import { getPathVariables } from './utils';

import SettingMenu from './Components/settings-menu';

//MATERIAL IMPORTS
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

//ORY IMPORTS
// The editor core
import Editor, { Editable } from 'ory-editor-core'
import 'ory-editor-core/lib/index.css' // we also want to load the stylesheets

// Require our ui components (optional). You can implement and use your own ui too!
import { Trash, DisplayModeToggle, Toolbar } from 'ory-editor-ui'
import 'ory-editor-ui/lib/index.css'



import { imageSlider, YikesSlate, ImageBay } from './my-plugins';


// may need css from default slate
// Load some exemplary plugins:
//import slate from 'ory-editor-plugins-slate' // The rich text area plugin
import 'ory-editor-plugins-slate/lib/index.css' // Stylesheets for the rich text area plugin
//for image slider
import './my-plugins/image-slider/index.css';
//for image bay
import './my-plugins/image-bay/index.css';


// The spacer plugin
import spacer from 'ory-editor-plugins-spacer'
import 'ory-editor-plugins-spacer/lib/index.css'

// The image plugin
import image from 'ory-editor-plugins-image'
import 'ory-editor-plugins-image/lib/index.css'

// The video plugin
import video from 'ory-editor-plugins-video'
import 'ory-editor-plugins-video/lib/index.css'

// The divider plugin
import divider from 'ory-editor-plugins-divider'

import parallax from 'ory-editor-plugins-parallax-background' // A plugin for parallax background images
import 'ory-editor-plugins-parallax-background/lib/index.css' // Stylesheets for parallax background images


import { HTMLRenderer } from 'ory-editor-renderer'

require('react-tap-event-plugin')() // react-tap-event-plugin is required by material-ui which is used by ory-editor-ui so we need to call it here

// Define which plugins we want to use. We only have slate and parallax available, so load those.
const plugins = {
  content: [YikesSlate(), spacer, image, video, divider, imageSlider, ImageBay], // Define plugins for content cells
  layout: [parallax({
    defaultPlugin: YikesSlate()
  })] // Define plugins for layout cells

}
console.log(ImageBay)


// Creates an empty editable

// Instantiate the editor

class App extends Component {

  constructor(props) {
    super(props);

    var windowUrl = window.location.href;

    this.state = {

    };

  }

  componentDidMount() {

    var {id, path, action, params} = getPathVariables()

    streamStart({
      id,
      path,
      action,
      params
    })

      .subscribe(content => {

        console.log('content', content, params)

        const editor = new Editor({

          plugins,
          // pass the content state - you can add multiple editables here
          editables: [content],

        })

        this.setState({
          id,
          path,
          action,
          params,
          editor,
          content
        })

      })

  }

  handleStateChange(state) {

    saveState(state)

  }

  render() {

    if (!this.state.editor) {

      return ( <p>
                 loading
               </p>)

    } else {

      return (

        <MuiThemeProvider>
          { this.state.action && this.state.action == "live" ?
            <HTMLRenderer state={ this.state.content } plugins={ plugins } />
            :
            <div>
              <SettingMenu/>
              <Editable editor={ this.state.editor } id={ this.state.content.id } onChange={ this.handleStateChange } />
              <Trash editor={ this.state.editor } />
              <DisplayModeToggle editor={ this.state.editor } />
              <Toolbar editor={ this.state.editor } />
            </div> }
        </MuiThemeProvider>

        );

    }

  } // render
}

export default App;