import React, { Component } from 'react';

import SettingsIcon from 'material-ui/svg-icons/action/settings';
import { postState } from '../../Services/ContentState'
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';

var settingsMenu = (props) => (
  <IconMenu className="settings-button" iconButtonElement={ <IconButton>
                                                            <SettingsIcon />
                                                          </IconButton> } anchorOrigin={ { horizontal: 'left', vertical: 'top' } } targetOrigin={ { horizontal: 'left', vertical: 'top' } }>
    <MenuItem onClick={ postState } primaryText="Save" />
    <MenuItem primaryText="DashBoad" />
    <MenuItem primaryText="Settings" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>

)

export default settingsMenu