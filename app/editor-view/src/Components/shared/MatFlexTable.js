import React, { Component } from 'react';
import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn,
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import uuidv4 from 'uuid/v4'

const styles = {
  propContainer: {
    width: 200,
    overflow: 'hidden',
    margin: '20px auto 0',
  },
  propToggleHeader: {
    margin: '20px auto 10px',
  },
};

export default class MatFlexTable extends Component {
  state = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: true,
    height: '300px',
  };

  handleToggle = (event, toggled) => {
    this.setState({
      [event.target.name]: toggled,
    });
  };

  handleChange = (event) => {
    this.setState({
      height: event.target.value
    });
  };

  onItemSelection = ({item, i, row}) => {
    this.props.onItemSelection({
      item,
      i,
      row
    })
  };

  render() {
    return (
      <div>
        <Table height={ this.state.height } fixedHeader={ this.state.fixedHeader } fixedFooter={ this.state.fixedFooter } selectable={ this.state.selectable } multiSelectable={ this.state.multiSelectable }>
          <TableHeader displaySelectAll={ this.state.showCheckboxes } adjustForCheckbox={ this.state.showCheckboxes } enableSelectAll={ this.state.enableSelectAll }>
            <TableRow>
              { this.props.tableRows.map((row, index) => (
                
                  <TableHeaderColumn key={ uuidv4() }>
                    { row.title }
                  </TableHeaderColumn>
                
                )) }
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={ this.state.showCheckboxes } deselectOnClickaway={ this.state.deselectOnClickaway } showRowHover={ this.state.showRowHover } stripedRows={ this.state.stripedRows }>
            { this.props.items.map((item, i) => (
                <TableRow key={ uuidv4() }>
                  { this.props.tableRows.map((row, j) => (
                    
                      <TableRowColumn key={ uuidv4() }>
                        { item.cellStyle && item.cellStyle[row.key] ?
                          
                          
                          <div onClick={ (event) => {
                                         
                                           this.onItemSelection({
                                             item,
                                             i,
                                             row
                                           })
                                         
                                         } } style={ item.cellStyle[row.key] }>
                            { item[row.key] }
                          </div>
                          
                          
                          :
                          <div onClick={ (event) => {
                                         
                                           this.onItemSelection({
                                             item,
                                             i,
                                             row
                                           })
                                         
                                         } }>
                            { item[row.key] }
                          </div> }
                      </TableRowColumn>
                    
                    )) }
                </TableRow>
              )) }
          </TableBody>
        </Table>
      </div>
      );
  }
}