var defaultState = {
  "id": "b17811dc-8e5a-49ee-bf56-cf1710361635",
  "cells": [{
    "id": "b6ff61db-4d5b-4d42-8460-3de85a19772b",
    "inline": null,
    "size": 12,
    "rows": [{
      "id": "24cda4c9-5f6f-4e29-be9e-d4d6f6e189d1",
      "cells": [{
        "id": "8d0dc5b9-ee14-462f-8c16-c84a20722a83",
        "inline": null,
        "size": 12,
        "content": {
          "plugin": {
            "name": "ory/editor/core/content/spacer",
            "version": "0.0.1"
          },
          "state": {}
        }
      }]
    }, {
      "id": "79cc917d-2f76-422b-9e27-6d5dfa8ab066",
      "cells": [{
        "id": "10fdf244-c276-46be-b8a2-cb985ea69388",
        "inline": null,
        "size": 12,
        "content": {
          "plugin": {
            "name": "ory/editor/core/content/slate",
            "version": "0.0.1"
          },
          "state": {
            "serialized": {
              "nodes": [{
                "kind": "block",
                "type": "HEADINGS/HEADING-ONE",
                "nodes": [{
                  "kind": "text",
                  "text": ""
                }],
                "data": {
                  "fontFamily": "'Give You Glory'",
                  "align": "center"
                }
              }, {
                "kind": "block",
                "type": "HEADINGS/HEADING-ONE",
                "nodes": [{
                  "kind": "text",
                  "text": "Yikes you got site !"
                }],
                "data": {
                  "fontFamily": "'Give You Glory'",
                  "align": "center"
                }
              }, {
                "kind": "block",
                "type": "HEADINGS/HEADING-ONE",
                "nodes": [{
                  "kind": "text",
                  "text": ""
                }],
                "data": {
                  "fontFamily": "'Give You Glory'",
                  "align": "center"
                }
              }]
            }
          }
        }
      }]
    }, {
      "id": "ae242838-0acd-4a2b-aee8-4864947ab778",
      "cells": [{
        "id": "85a19723-8d41-4806-9340-d1e7a536c635",
        "inline": null,
        "size": 3,
        "content": {
          "plugin": {
            "name": "ory/editor/core/content/spacer",
            "version": "0.0.1"
          },
          "state": {
            "height": 24
          }
        }
      }, {
        "id": "c14709cd-f35e-4899-9187-eb8f066dcd44",
        "inline": null,
        "size": 6,
        "content": {
          "plugin": {
            "name": "image-slider",
            "version": "0.0.1"
          },
          "state": {
            "images": [{
              "src": "https://storage.googleapis.com/yikes-sites/demo-images/Blyde_River_Canyon%2C_South_Africa_1.JPG"
            }, {
              "src": "https://storage.googleapis.com/yikes-sites/demo-images/Boulders_Beach_1_HDR.jpg"
            }, {
              "src": "https://storage.googleapis.com/yikes-sites/demo-images/Cape_Town_Sunset_HDR.jpg"
            }]
          }
        }
      }, {
        "id": "44ca9ae2-4fdd-494d-8985-7a6e758793fb",
        "inline": null,
        "size": 3,
        "content": {
          "plugin": {
            "name": "ory/editor/core/content/spacer",
            "version": "0.0.1"
          },
          "state": {
            "height": 24
          }
        }
      }]
    }, {
      "id": "b13411e0-dd88-481f-81cd-12fdfe8e8071",
      "cells": [{
        "id": "bb7ab3c2-16db-49e2-b8ab-fac58183ceb3",
        "inline": null,
        "size": 12,
        "content": {
          "plugin": {
            "name": "ory/editor/core/content/spacer",
            "version": "0.0.1"
          },
          "state": {
            "height": 61
          }
        }
      }]
    }]
  }]
}

export { defaultState }