import { createEmptyState } from 'ory-editor-core'

import { Observable } from 'rxjs'
import restService from '../restService'
import { defaultState } from './deafultState'

import * as YikesContext from '../../yikesContext/'

var localStorageStateKey = "state";
export var localStorageEditoPageDataKey = "editorPageData";



var postState = function() {

  var localPageData = localStorage.getItem(localStorageEditoPageDataKey);
  var localStateData = localStorage.getItem(localStorageStateKey);

  var parsedPageData = JSON.parse(localPageData)

  var parsedStateData = JSON.parse(localStateData)


  return restService.put(`${pageDatUrlBase}/${pageApiPath}`, {
    ...parsedPageData,
    editState: parsedStateData
  })

    .subscribe((postPageDataResponse) => {

      console.log('postPageDataResponse', postPageDataResponse)

    })

}

var loadState = function() {

  var content = defaultState;

  var savedStateString = localStorage.getItem(localStorageStateKey);

  if (savedStateString && savedStateString.length > 0) {

    content = JSON.parse(savedStateString)

  }

  return content

};

var saveState = function(state) {

  console.log('state change', state)
  localStorage.setItem(localStorageStateKey, JSON.stringify(state));

}

export var pageDatUrlBase = window.location.port == 3000 ? 'http://localhost:8080' : '';
export var pageApiPath = 'site-pages'

var streamStart = function(config?) {

  var state = loadState();

  console.log('config config config', config)

  if (config && config.id && config.action == 'editor' || config.action == 'live') {

    return restService.get(`${pageDatUrlBase}/${pageApiPath}?_id=${config.id}`)

      .map((getPageDataResponse) => {

        console.log('getPageDataResponse', getPageDataResponse)

        localStorage.setItem(localStorageEditoPageDataKey, JSON.stringify(getPageDataResponse));

        var {editState, siteId} = getPageDataResponse

        YikesContext.setUserSiteContext(siteId)

        if (getPageDataResponse && editState) {

          return getPageDataResponse.editState

        }

        return state

      })

  }

  return Observable.of(state)
}






export { loadState, saveState, streamStart, postState }













