const Rx = require('rxjs');

var restStorageKey = 'workChew.rest'

var getAccessToken = () => localStorage.getItem('yikes-editor-access-token')

var restService = {

  restStorageKey,

  get: (url) => {

    return Rx.Observable.fromPromise(fetch(url, {

      method: "GET",

      headers: {
        "x-api-access-token": getAccessToken()
      }

    }).then((res) => res.json()))

  },
  delete: (url) => {

    return Rx.Observable.fromPromise(fetch(url, {

      method: "DELETE",
      headers: {
        "x-api-access-token": getAccessToken()
      }

    }).then((res) => res.json()))

  },
  postForm: (url, form) => {

    return Rx.Observable.fromPromise(fetch(url, {

      method: "POST",

      headers: {
        "x-api-access-token": getAccessToken()
      },

      body: form

    })

      .then((res) => res.json()))

  },

  post: (url, body) => {

    return Rx.Observable.fromPromise(fetch(url, {

      method: "POST",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": getAccessToken()
      },

      body: JSON.stringify(body)

    })

      .then((res) => res.json()))

  },
  put: (url, body) => {

    return Rx.Observable.fromPromise(fetch(url, {

      method: "PUT",

      headers: {
        "Content-type": "application/json",
        "x-api-access-token": getAccessToken()

      },

      body: JSON.stringify(body)

    })

      .then((res) => res.json()))

  }
}

export default restService