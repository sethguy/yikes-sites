/*
 * This file is part of ORY Editor.
 *
 * ORY Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * ORY Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License
 * along with ORY Editor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LGPL-3.0
 * @copyright 2016-2018 Aeneas Rekkas
 * @author Aeneas Rekkas <aeneas+oss@aeneas.io>
 *
 */

// @flow

import * as YikesContext from '../../../../yikesContext'
import restService from '../../../../Services/restService.js';


import { v4 } from 'uuid'
import React from 'react'
import Display from '../Display'
import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import type { PropTypes } from '../index.js'

import ContentRemove from 'material-ui/svg-icons/content/remove';

import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';

import { BottomToolbar } from 'ory-editor-ui'

import RaisedButton from 'material-ui/RaisedButton';


var uploadedPreviews = [];


var savedUserImages = [];


const handleChange = (props, event, i) => {
  const target = event.target
  if (target instanceof HTMLInputElement) {
    const change = {}

    var images = [...props.state.images];

    console.log('on change', target.value)
    images[i].src = target.value;

    props.onChange({
      images
    })

  }
}

const add = (props) => {

  var nuImages = [{
    src: ""
  }]

  if (props.state.images) {

    nuImages = [...props.state.images, {
      src: ""
    }]

  }

  props.onChange({
    images: nuImages
  })

}

const remove = (props, i) => {

  var nuImages = []

  if (props.state.images) {

    var images = [...props.state.images]

    images.splice(i, 1)
    nuImages = [... images]

  }

  props.onChange({
    images: nuImages
  })

}

var clearPreviewImage = (props) => {

  uploadedPreviews = []
  var input = document.getElementById('yikes-img-bay-upload-input')

  input.files = null
  props.onChange({
    key: v4()
  })

}

var saveImageToRepo = (props) => {

  var input = document.getElementById('yikes-img-bay-upload-input')
  var files = input.files;
  var picForm = new FormData()

  var imgBlob = new Blob([files[0]], {
    type: "image/jpeg"
  });

  picForm.append('file', imgBlob);

  YikesContext.uploadPic(picForm)

    .subscribe((postFormStream) => {
      var images = [];
      YikesContext.addStoredImages([postFormStream.postPicResponse])
      uploadedPreviews = []

      console.log('postFormStream', postFormStream)
      input.files = null

      if (props.state.images) {
        images = props.state.images

      }

      images = [...images, {
        href: `${YikesContext.imgSrcPrefix()}${postFormStream.postPicResponse._id}`
      }]
      props.onChange({
        key: v4(),
        images
      })

    })

}

var onImageUpload = (event, props) => {

  var {files} = event.target;
  var reader = new FileReader();

  reader.onload = function(e) {
    var dataUrl = e.target.result;

    uploadedPreviews = [{
      src: dataUrl
    }]

    props.onChange({
      key: v4()
    })

  }

  reader.readAsDataURL(files[0]);

}


const ImageList = (props) => {


  console.log("these props", props)

  return (

    <div className="img-bay-form-image-list-scroll row">
      { props.state.images ? props.state.images.map((imageData, i) => (
        
          <div key={ v4() } className="img-bay-image-src-container col-md-6">
            <img src={ imageData.href } />
          </div>
        
        )) : "" }
    </div>)
}

const Form = (props: PropTypes) => (
  <div>
    <Display {...props} />
    { props.focused &&
      <div className="img-bay-form d-flex flex-column" style={ { position: "fixed", height: "100%", left: 0, top: 0, backgroundColor: "white", zIndex: 500 } }>
        <br/>
        <div className="img-slider-form-actions d-flex justify-content-center">
          <RaisedButton className="w-75" containerElement='label' onClick={ (event) => {
                                                                              clearPreviewImage(props)
                                                                            } }>
            Upload Image
            <input id="yikes-img-bay-upload-input" onChange={ (event) => {
                                                                onImageUpload(event, props)
                                                              } } style={ { position: "absolute", visibility: "hidden" } } type="file" />
          </RaisedButton>
        </div>
        <br/>
        <div className="img-slider-form-actions d-flex justify-content-center">
          <RaisedButton className="w-75" onClick={ (event) => {
                                                     clearPreviewImage(props)
                                                   } }>
            Select From image Repo
          </RaisedButton>
        </div>
        <br/>
        <div className="img-bay-form-image-list-container d-flex flex-column">
          <div className="img-bay-upload-preview-area d-flex justify-content-center scroll-y w-100">
            { uploadedPreviews && uploadedPreviews.map((img, i) => (
                <img className="h-100" key={ v4() } src={ img.src } />
              )) }
          </div>
          <br/>
          { uploadedPreviews.length > 0 &&
            
            <div className="w-100 d-flex justify-content-around">
              <RaisedButton onClick={ (event) => {
                                        saveImageToRepo(props)
                                      } }>
                save
              </RaisedButton>
              <RaisedButton onClick={ (event) => {
                                        clearPreviewImage(props)
                                      } }>
                cancel
              </RaisedButton>
            </div> }
          <br/>
          <ImageList {...props}/>
        </div>
      </div> }
  </div>
)

export default Form
