/*
 * This file is part of ORY Editor.
 *
 * ORY Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * ORY Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License
 * along with ORY Editor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LGPL-3.0
 * @copyright 2016-2018 Aeneas Rekkas
 * @author Aeneas Rekkas <aeneas+oss@aeneas.io>
 *
 */

// @flow
import React from 'react';
import ImageIcon from 'material-ui/svg-icons/image/panorama';

import { iconStyle } from '../common.js';
import type { PropTypes } from '../index.js';

import { v4 } from 'uuid';



const Display = ({isEditMode, state }:PropTypes) => {

  return (
    <div id="carouselExampleControls" className="carousel slide" style={ { minHeight: 300, backgroundColor: "black" } } data-ride="carousel">
      <div className="carousel-inner">
        { state.images ? state.images.map((imageInput, i) => (
            <div key={ v4() } className={ i == 0 ? "carousel-item active" : "carousel-item" }>
              { (imageInput.src && imageInput.src.length > 0) ? <img className="slider-image" src={ imageInput.src } /> : <ImageIcon style={ iconStyle } /> }
            </div>)) :
          <div></div> }
      </div>
    </div>


  )
}

export default Display
