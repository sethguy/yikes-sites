/*
 * This file is part of ORY Editor.
 *
 * ORY Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * ORY Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License
 * along with ORY Editor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LGPL-3.0
 * @copyright 2016-2018 Aeneas Rekkas
 * @author Aeneas Rekkas <aeneas+oss@aeneas.io>
 *
 */

// @flow

import { v4 } from 'uuid'
import React from 'react'
import Display from '../Display'
import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import type { PropTypes } from '../index.js'

import ContentRemove from 'material-ui/svg-icons/content/remove';

import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';

import { BottomToolbar } from 'ory-editor-ui'



const handleChange = (props, event, i) => {
  const target = event.target
  if (target instanceof HTMLInputElement) {
    const change = {}

    var images = [...props.state.images];

    console.log('on change', target.value)
    images[i].src = target.value;

    props.onChange({
      images
    })

  }
}

const add = (props) => {

  var nuImages = [{
    src: ""
  }]

  if (props.state.images) {

    nuImages = [...props.state.images, {
      src: ""
    }]

  }

  props.onChange({
    images: nuImages
  })

}

const remove = (props, i) => {

  var nuImages = []

  if (props.state.images) {

    var images = [...props.state.images]

    images.splice(i, 1)
    nuImages = [... images]

  }

  props.onChange({
    images: nuImages
  })

}

const ImageSrcList = (props) => (

  <div className="img-slider-form-image-list-scroll">
    { props.state.images ? props.state.images.map((imageInput, i) => (
      
        <div key={ v4() } className="img-slider-form-image-src-container">
          <TextField
                     hintText="img src"
                     floatingLabelText="Image location (url)"
                     inputStyle={ { color: 'white' } }
                     floatingLabelStyle={ { color: 'white' } }
                     hintStyle={ { color: 'grey' } }
                     name={ `image-${i}` }
                     style={ { width: '512px' } }
                     defaultValue={ imageInput.src }
                     onChange={ (event) => handleChange(props, event, i) } />
          <FloatingActionButton onClick={ () => remove(props, i) } mini={ true }>
            <ContentRemove />
          </FloatingActionButton>
        </div>
      
      )) : "" }
  </div>)

const Form = (props: PropTypes) => (
  <div>
    <Display {...props} />
    <BottomToolbar open={ props.focused }>
      <div className="img-slider-form-actions">
        <FloatingActionButton onClick={ () => add(props) } mini={ true }>
          <ContentAdd />
        </FloatingActionButton>
        <span>Add Image</span>
        { props.state.images && <span>{ props.state.images.length }</span> }
      </div>
      <div className="img-slider-form-image-list-container">
        <ImageSrcList {...props}/>
      </div>
    </BottomToolbar>
  </div>
)

export default Form
