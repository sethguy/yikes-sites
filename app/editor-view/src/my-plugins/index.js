import imageSlider from './image-slider'
import YikesSlate from './yikes-slate'
import ImageBay from './image-bay'

export { imageSlider, YikesSlate, ImageBay }