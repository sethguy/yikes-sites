import React, { Component } from 'react';

import MatFlexTable from '../../Components/shared/MatFlexTable'

import { loadFonts, intiGoogleFontsPlugin, storeFontList, getGoogleFontList, getGoogleFontPage } from './googleFonts/'

import FlatButton from 'material-ui/FlatButton'

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';


import NavigationChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';

import NavigationChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

import IconButton from 'material-ui/IconButton';

const tableRows = [{
  title: "Family",
  key: "family"
}, {
  title: "Category",
  key: "category"
}, {
  title: "Preview",
  key: "preview"
}]

const PageDrop = (props) => (
  <DropDownMenu style={ { width: 150 } } value={ props.selectedValue } onChange={ props.handleChange }>
    { [5, 10, 20].map((pageCount, j) => (<MenuItem key={ j } value={ j } primaryText={ pageCount } />)) }
  </DropDownMenu>
)

export default class MatFontFlexTable extends Component {

  constructor(props) {

    super(props);

    this.state = {
      ...this.props,
      fonts: [],
      pageDrop: {
        selectedValue: 0
      },
      searchTerm: "",
      pageCounts: [5, 10, 20],
      pageSelection: 0,
      skip: 0,
      fontListLength: getGoogleFontList().length,
      searchResults: []
    }

  }

  componentDidMount() {

    intiGoogleFontsPlugin()

      .subscribe((allFonts) => {


      })

    this.getFontPage()

  }

  getFontPage() {

    var top = this.state.pageCounts[this.state.pageDrop.selectedValue]
    var skip = (this.state.pageSelection * top)

    var {searchTerm} = this.state;

    if (searchTerm && searchTerm.length) {

      loadFonts(this.getSearchResultsPage(top, skip))

        .subscribe((fonts) => {

          this.setState({
            fonts
          })

        })

    } else {

      loadFonts(getGoogleFontPage(top, skip))

        .subscribe((fonts) => {

          this.setState({
            fonts
          })

        })

    }

  }

  getSearchResultsPage(top, skip) {

    return this.state.searchResults.filter((font, i) => {

      return (i >= skip) && (i < (skip + top))

    })

  }

  handleToggle = (event, toggled) => {
    this.setState({
      [event.target.name]: toggled,
    });
  };

  handleChange = (event) => {


  };

  updatePageDrop(event, index, value) {

    this.setState({
      ...this.state,
      pageDrop: {
        ...this.state.pageDrop,
        selectedValue: value
      }
    }, () => {


      this.getFontPage()

    })

  }

  onItemSelection = (event) => {

    this.props.onFontSelection(event)

  }

  onPageSelection = ({pageSelection}) => {

    var {top, skip} = this.getTopSkip()

    var chop = Math.floor(this.state.fontListLength / top) + 1

    if (pageSelection > -1 && pageSelection < chop) {

      this.setState({
        pageSelection
      }, () => {

        this.getFontPage()
      })

    }

  }

  getSkipLabel() {

    var itemsPerPage = this.state.pageCounts[this.state.pageDrop.selectedValue]
    var amount = (this.state.pageSelection * itemsPerPage)

    return ` ${amount+1} - ${amount+itemsPerPage}`

  }

  getTopSkip() {

    var top = this.state.pageCounts[this.state.pageDrop.selectedValue]
    var skip = (this.state.pageSelection * top)

    return {
      top,
      skip
    }

  }

  mapFontsForTable() {

    return this.state.fonts.map(font => {
      return {
        ...font,
        preview: "a b c 1 2 3",
        cellStyle: {
          preview: {
            fontFamily: `'${font.family}'`,
            fontSize: "30px"
          }
        }
      }
    })
  }

  updateSearch = (event) => {

    var searchTerm = event.target.value;

    var fonts = getGoogleFontList().filter((font) => /*(font.category.indexOf(searchTerm) > -1) || */ (font.family.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1))

    this.setState({
      searchTerm,

      fontListLength: fonts.length,
      pageDrop: {
        selectedValue: 0
      },
      pageSelection: 0,
      searchResults: fonts
    }, () => {

      this.getFontPage()

    })

  }

  render() {
    return (<div>
              <TextField placeholder={ 'search' } id={ "mat-font-table-search" } onChange={ this.updateSearch } value={ this.state.searchTerm } />
              <MatFlexTable items={ this.mapFontsForTable() } onItemSelection={ (event) => {
                                                                                  this.onItemSelection(event)
                                                                                } } tableRows={ tableRows } />
              <div className="pagination d-flex justify-content-end align-items-center">
                <div className="d-flex align-items-center">
                  items per page
                  <PageDrop {...this.state.pageDrop} handleChange={ (event, index, value) => {
                                                                    
                                                                      this.updatePageDrop(event, index, value)
                                                                    } } />
                </div>
                <div className="d-flex">
                  { this.getSkipLabel() }
                  { '  of  ' }
                  { this.state.fontListLength }
                </div>
                <div className="d-flex justify-content-around">
                  <IconButton>
                    <NavigationChevronLeft onClick={ (event) => {
                                                     
                                                       this.onPageSelection({
                                                         pageSelection: this.state.pageSelection - 1
                                                       })
                                                     
                                                     } } />
                  </IconButton>
                  <IconButton>
                    <NavigationChevronRight onClick={ (event) => {
                                                      
                                                        this.onPageSelection({
                                                          pageSelection: this.state.pageSelection + 1
                                                        })
                                                      
                                                      } } />
                  </IconButton>
                </div>
              </div>
            </div>);
  }

}


