import { Observable } from 'rxjs'
import WebFont from 'webfontloader';

import Rx from 'rxjs'

var googleFontsUrl = "https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBZP8C0SQyYJS5RToD3xqO3Bmw9Oj7m9fk";

var googleFontListStorageKey = "googleFontListStorage";

var selectedFontStorageKey = "selectedFontStorage"

var loadedFontsStorageKey = "loadedFontsStorage"

var createFontUrl = function(typeletinfo) {

  var list = typeletinfo.map((info) => info.style.fontFamily).reduce((acc, nextFont) => {

    if (acc.indexOf(nextFont) == -1) return acc + "|" + nextFont
    else return acc;

  }, "");

  return "https://fonts.googleapis.com/css?family=" + list.substring(1)

}

var loadFonts = function(pickedFonts) {

  console.log("at load fonts ", pickedFonts)

  if (pickedFonts.length) return Observable.create((observer) => {

      var newfams = pickedFonts.map(font => font.family)

      WebFont.load({

        google: {
          families: newfams
        },

        active: function() {

          observer.next(pickedFonts)

        }

      });

    })

  return Rx.Observable.of(pickedFonts)

}

var getLoadedFonts = function() {

  return JSON.parse(localStorage.getItem(loadedFontsStorageKey) || '[]')

}

var storeLoadedFont = function(font) {

  var loadedFonts = getLoadedFonts(loadedFontsStorageKey)

  if (font && font.family) {

    var fontSearch = loadedFonts.filter(font => font.family == font.family)

    if (fontSearch.length == 0) {

      loadedFonts.push(font)

      localStorage.setItem(loadedFontsStorageKey, JSON.stringify(loadedFonts))

    } else {


    }

  }

  return loadedFonts;

}

var getGoogleFontsListJson = function() {

  return Observable.fromPromise(fetch(googleFontsUrl))

    .switchMap(getFontListResponse => getFontListResponse.json())

}

var storeFontList = function(fonts) {

  localStorage.setItem(googleFontListStorageKey, JSON.stringify(fonts))

  return fonts
}

var getGoogleFontListData = function() {

  return JSON.parse(localStorage.getItem(googleFontListStorageKey) || '{}')
}

var getGoogleFontList = function() {

  //.filter((x, i) => i < 28)
  return getGoogleFontListData().items || []
}

var getGoogleFontPage = function(top = 10, skip = 0) {

  return getGoogleFontList()

    .filter((font, i) => {

      return (i >= skip) && (i < (skip + top))

    })

}

var intiGoogleFontsPlugin = function() {

  var storedFontList = getGoogleFontList()

  if (storedFontList.length == 0) {

    return getGoogleFontsListJson()

      .map((fontList) => {

        return storeFontList(fontList)

      })

  } else {

    return Rx.Observable.of(storedFontList)

  }

}

var setSelectedFonts = function(fonts) {
  localStorage.setItem(selectedFontStorageKey, JSON.stringify(fonts))

  return fonts;

}

var getSelectedFonts = function() {

  return JSON.parse(localStorage.getItem(selectedFontStorageKey) || '[]')

}

var storeSelectedFont = function(font) {

  var selectedFonts = getSelectedFonts(selectedFontStorageKey).filter(font => font.family == font.family)

  selectedFonts.push(font)

  localStorage.setItem(selectedFontStorageKey, JSON.stringify(selectedFonts))

  return selectedFonts;

}



export { loadFonts, getGoogleFontList, getGoogleFontsListJson, intiGoogleFontsPlugin, storeFontList, getGoogleFontPage }


