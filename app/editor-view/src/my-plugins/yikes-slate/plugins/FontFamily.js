/*
 * This file is part of ORY Editor.
 *
 * ORY Editor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * ORY Editor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public License
 * along with ORY Editor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LGPL-3.0
 * @copyright 2016-2018 Aeneas Rekkas
 * @author Aeneas Rekkas <aeneas+oss@aeneas.io>
 *
 */

/* eslint-disable prefer-reflect */
import FontDownLoad from 'material-ui/svg-icons/content/font-download'
import React, { Component } from 'react'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { ToolbarButton } from '../helpers'
import Plugin from './Plugin'
import type { Props } from './props'

import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import MatFontFlexTable from '../MatFontFlexTable'


const FontDrop = (props) => (<div>
                               <DropDownMenu style={ { width: 200 } } value={ props.selectedFont } onChange={ props.handleChange }>
                                 { props.fonts.map((font, j) => (<MenuItem key={ font.name } value={ j } primaryText={ font.name } />)) }
                               </DropDownMenu>
                             </div>

)

class Button extends Component {
  state = {
    open: false,
    href: '',
    title: '',
    hadLinks: false,
    selectedFont: 0
  }

  props: Props

  input: Component<*, *, *>

  onRef = (component: Component<*, *, *>) => {

  }

  handleSubmit = () => {

    this.setState({
      open: false
    })

    var {onChange, editorState} = this.props;

    var oldData = editorState.blocks.reduce((data, block) => {

      return {
        fontFamily: block.data.get('fontFamily'),

        align: block.data.get('align')
      }

    }, {})

    onChange(
      editorState
        .transform()
        .setBlock({
          data: {
            ...oldData,
            fontFamily: this.state.selectedFont
          }
        })
        .apply()
    )

  } //handleSubmit

  onFontSelection = (fontSelectionEvent) => {

    console.log('fontSelectionEvent', fontSelectionEvent)

    this.setState({
      selectedFont: `'${fontSelectionEvent.item.family}'`,
    })

  }

  onClick = e => {
    const {editorState, onChange} = this.props
    e.preventDefault()

    this.setState({
      open: !this.state.open
    })

  }

  handleClose = () => {
    this.setState({
      open: false
    })

  }

  render() {
    const actions = [
      <FlatButton key="0" label="Cancel" primary onTouchTap={ this.handleClose } />,
      <FlatButton key="1" label="Submit" primary onTouchTap={ this.handleSubmit } />
    ]

    const {editorState} = this.props

    return (
      <MuiThemeProvider muiTheme={ getMuiTheme() }>
        <Notspan>
          <ToolbarButton onClick={ this.onClick } isActive={ false } icon={ <FontDownLoad /> } />
          <Dialog className="ory-prevent-blur" title="Select a Font" modal={ false } open={ this.state.open } actions={ [actions] }>
            <MatFontFlexTable onFontSelection={ this.onFontSelection } />
          </Dialog>
        </Notspan>
      </MuiThemeProvider>
    )
  }
}

const Notspan = (props) => (<span>{ props.children }</span>)

export default class FontFamilyPlugin extends Plugin {

  props: Props

  name = 'fontFamily'

  toolbarButtons =[
    Button

  ]

}
