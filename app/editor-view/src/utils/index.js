var getQueryParams = function() {

  var url = window.location.href;
  console.log('url', url)

  var queryString = url.substring(url.indexOf('?') + 1)

  if (url.indexOf('?') > -1) {

    var splits = queryString.split('&')

    var queryParams = splits

      .map(split => split.split('='))

      .map(([name, value]) => {

        return {

          [name]: value
        }

      })

      .reduce((params, splitItem) => {

        return {
          ...params,
          ...splitItem
        }

      }, {})

    console.log('queryParams', queryParams)

    return queryParams

  }

}

var getPathVariables = () => {

  var segments = window.location.pathname.split('/');

  if (segments[0] == "" && segments[1] == "") {

    var hash = window.location.hash;

    var query = hash.indexOf('?')

    var segmentsLength = (query > -1) ? query : (hash.length + 1)

    segments = hash.substring(0, segmentsLength).split('/');


  }

  var [path, action, id] = segments;

  if (!id) {

    id = action

  }

  var params = getQueryParams()

  if (params && params['access-token']) {

    localStorage.setItem('yikes-editor-access-token', params['access-token'])
  } else {
    localStorage.removeItem('yikes-editor-access-token')

  }

  return {
    path,
    action,
    id,
    params
  }

}

export { getQueryParams, getPathVariables }