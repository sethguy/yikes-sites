import restService from '../Services/restService'

import { pageDatUrlBase, pageApiPath, localStorageEditoPageDataKey } from '../Services/ContentState/'

const LocalStorageYikesUserContextKey = "YikesUserContext"

const LocalStorageYikeImageDataStoreKey = "YikeImageDataStore"

export var imgSrcPrefix = ()=> `${pageDatUrlBase}/images/`

export var getUserContextData = function() {

  var context = JSON.parse(localStorage.getItem(LocalStorageYikesUserContextKey));

  return context || {}

}

export var updateUserContextData = function(contextData) {

  var context = getUserContextData()

  var nuData = {
    context,
    ...contextData
  }

  localStorage.setItem(LocalStorageYikesUserContextKey, JSON.stringify(nuData))

}


export var setUserContextData = function(contextData) {

  localStorage.setItem(LocalStorageYikesUserContextKey, JSON.stringify(contextData))

}


export var getUserSitePages = function(siteId) {

  var getSitePagesUrl = `${pageDatUrlBase}/${pageApiPath}?siteId=${siteId}`

  return restService.get(getSitePagesUrl)


}

export var setUserSiteContext = function(siteId) {

  getUserSitePages(siteId)

    .switchMap((userSitePagesResponse) => {

      setUserContextData({
        sitePages: userSitePagesResponse
      })

      return getImageRepoPage({
        siteId
      })

    })

    .subscribe((imageRepoData) => {

      setStoredImages(imageRepoData.map(image => {

        image.href = `${pageDatUrlBase}/images/${image._id}`

        return image

      }))

    })

}

export var setStoredImages = function(imageDataList) {

  return localStorage.setItem(LocalStorageYikeImageDataStoreKey, JSON.stringify(imageDataList))

}


export var addStoredImages = function(imagesToAdd) {

  console.log(imagesToAdd)

  var imageDataList = [...getStoredImages(), ...imagesToAdd]
  console.log(imageDataList)

  return localStorage.setItem(LocalStorageYikeImageDataStoreKey, JSON.stringify(imageDataList.map(image => {

    image.href = `${pageDatUrlBase}/images/${image._id}`

    return image

  })))

}

export var getStoredImages = function() {

  return JSON.parse(localStorage.getItem(LocalStorageYikeImageDataStoreKey) || '[]')

}


export var getImageRepoPage = ({siteId, pageId, userId}) => {

  var imagesUrl = `${pageDatUrlBase}/images?metadata.site=${siteId}`;

  var localPageData = localStorage.getItem(localStorageEditoPageDataKey);

  var parsedPageData = JSON.parse(localPageData)

  return restService.get(imagesUrl)

}

export var uploadPic = function(picForm) {

  var imagesUrl = `${pageDatUrlBase}/images`;

  var localPageData = localStorage.getItem(localStorageEditoPageDataKey);

  var parsedPageData = JSON.parse(localPageData)

  var {_id, siteId} = parsedPageData

  console.log('parsedPageData', parsedPageData)

  picForm.append('page', _id);

  picForm.append('site', siteId);

  return restService.postForm(imagesUrl, picForm)

// body...
}

export var yikesLinkPages = function() {

  var context = getUserContextData()

  var {sitePages} = context;

  return sitePages || [];

}
