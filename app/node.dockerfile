FROM node:latest

MAINTAINER Seth Terry

ENV NODE_ENV=development 

ENV NODE_HTTP_PORT=8080

ENV NODE_HTTPS_PORT=8080

ENV MONGO_IP=mongodb

RUN npm install forever -g

COPY package.json /yikes-site/package.json

RUN cd yikes-site; npm install

ADD server.js /yikes-site/server.js

ADD yikes-dashboard/build /yikes-site/yikes-dashboard/build

ADD editor-view/build /yikes-site/editor-view/build

ADD routes /yikes-site/routes

ADD middleWare /yikes-site/middleWare

ADD rxFormidable /yikes-site/rxFormidable

ADD NodeServices /yikes-site/NodeServices

WORKDIR   /yikes-site

EXPOSE $NODE_HTTP_PORT

EXPOSE $NODE_HTTPS_PORT

ENTRYPOINT ["forever", "server.js"]