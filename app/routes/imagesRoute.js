
const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const pic = require('../NodeServices/imageService.js')

const parseReqForm = require('../rxFormidable/')

router.post('/', (req, res) => {

  return parseReqForm(req)

    .subscribe(({fields, files, fileList}) => {

      console.log('fields, files, fileList', {
        fields,
        files,
        fileList
      })

      var {user, site, page} = fields;

      var metadata = {
        user,
        site,
        page,
        created: new Date().getTime()
      }

      pic

        .post({

          file: fileList[0],

          metadata
        })

        .subscribe((postPicResponse) => {

          res.send({
            postPicResponse
          })

        })

    })

}); //POST 

router.get('/:id', (req, res) => {

  return pic

    .getById({
      _id: req.params.id
    })

    .subscribe((gfs) => {

      res.writeHead(200, {
        'Content-Type': 'image/png'
      });

      gfs.pipe(res);

    })

})

router.get('/', (req, res) => {

  return pic

    .get(req.query)

    .subscribe((getPicResponse) => {

      res.send(getPicResponse)


    })

})

module.exports = router;