const express = require('express');

const router = express.Router();

var path = require('path');

var fs = require('fs');

var routes = [
  {
    name: 'health'
  },
  {
    name: 'images'
  },
  {
    name: 'users'
  },
  {
    name: 'siteData',
    path: 'site-data'
  },
  {
    name: 'sitePages',
    path: 'site-pages'
  },
]

routes.forEach((routeConfig) => {

  var routePath = `/${routeConfig.path || routeConfig.name}`;

  var routeSrcPath = `./${routeConfig.name}Route`;

  router.use(routePath, require(routeSrcPath))

})

// GET home page
router.get('/', (req, res) => {

  res.set('Content-Type', 'text/html');

  res.send(fs.readFileSync('./yikes-dashboard/build/index.html'));

});

router.get('/live/:pageId', (req, res) => {


  res.set('Content-Type', 'text/html');

  res.send(fs.readFileSync('./editor-view/build/index.html'));

});


router.get('/editor/:pageId', (req, res) => {


  res.set('Content-Type', 'text/html');

  res.send(fs.readFileSync('./editor-view/build/index.html'));

});

router.post('/', (req, res) => {

  console.log(req.body)
  res.send(req.body);

});

module.exports = router;