const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const sitePages = require('../NodeServices/sitePagesService.js')

const siteData = require('../NodeServices/siteDataService.js')

router.post('/', (req, res) => {

  console.log('req.body @ siteData post route', req.body)

  siteData

    .post(req.body)

    .subscribe((postSitedataResponse) => {

      res.send({
        postSitedataResponse
      })

    })

}); //POST 

router.put('/', (req, res) => {

  console.log('req.body @ siteData put route', req.body)

  siteData

    .update(req.body)

    .subscribe((putSitedataResponse) => {

      res.send({
        putSitedataResponse
      })

    })

}) //PUT

router.get('/', (req, res) => {

  return siteData

    .get(req.query)

    .subscribe((getSitedataResponse) => {

      if (req.query._id) {


        res.send({
          siteData: getSitedataResponse
        })

      } else {

        res.send(getSitedataResponse)

      }


    })

})

router.get('/pages/:id', (req, res) => {

  var {id} = req.params;

  return sitePages

    .get({
      siteId: id
    })

    .subscribe((getUserResponse) => {

      res.send(getUserResponse)

    })

})

router.delete('/', (req, res) => {

  return siteData

    .delete(req.query)

    .subscribe((getSitedataResponse) => {

      res.send(getSitedataResponse)

    })

})


module.exports = router;