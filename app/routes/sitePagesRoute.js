const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const sitePages = require('../NodeServices/sitePagesService.js')

router.post('/', (req, res) => {

  console.log('req.body @ sitePages post route', req.body)

  sitePages

    .post(req.body)

    .subscribe((postSitepagesResponse) => {

      res.send({
        postSitepagesResponse
      })

    })

}); //POST 

router.put('/', (req, res) => {

  console.log('req.body @ sitePages put route', req.body)

  sitePages

    .put(req.body)

    .subscribe((putSitepagesResponse) => {
      console.log('rputSitepagesResponse putSitepagesResponse put route', putSitepagesResponse)

      res.send({
        putSitepagesResponse
      })

    })

}) //PUT

router.get('/', (req, res) => {

  return sitePages

    .get(req.query)

    .subscribe((getSitepagesResponse) => {

      res.send(getSitepagesResponse)

    })

})


router.delete('/', (req, res) => {

  return sitePages

    .delete(req.query)

    .subscribe((getSitepagesResponse) => {

      res.send(getSitepagesResponse)

    })

})


module.exports = router;