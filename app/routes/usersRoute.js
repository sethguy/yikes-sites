const Rx = require('rxjs')

const express = require('express');

const router = express.Router();

const user = require('../NodeServices/userService.js')
const sites = require('../NodeServices/siteDataService.js')

const uuidv4 = require("uuid/v4")

router.post('/', ({body}, res) => {

  res.set('Content-Type', 'text/html');

  if (body.login) {

    user

      .get(body.login)

      .switchMap((userLoginResponse) => {

        var foundUser = userLoginResponse[0];
        console.log("found user resp", foundUser)
        foundUser.auth = {
          accessToken: uuidv4(),
          date: new Date()
        }

        return user

          .update(foundUser)
      })

      .map((updateUserInfo) => {

        var visible = Object.assign({}, updateUserInfo)

        visible.accessToken = visible.auth.accessToken;

        delete visible.auth;
        delete visible.password;

        return visible;

      })

      .subscribe((userLoginResponse) => {

        var responseBody = {
          userInfo: userLoginResponse
        }

        res.json(responseBody)

      })

  } else {

    user

      .post(userFromBody(body))

      .subscribe((userPostResponse) => {

        var responseBody = {
          userResponse: userPostResponse
        }

        res.json(responseBody)

      })

  }

}); //POST 

router.put('/', ({body}, res) => {

  res.set('Content-Type', 'text/html');

  user

    .update(userFromBody(body))

    .subscribe((userPutResponse) => {

      var responseBody = {
        userResponse: userPutResponse
      }

      res.send(responseBody)

    })

}); //PUT 

router.get('/', (req, res) => {

  return user

    .get(req.query)

    .subscribe((getUserResponse) => {

      res.send(getUserResponse)

    })

})

router.get('/sites/:id', (req, res) => {

  var {id} = req.params;

  return sites

    .get({
      user: id
    })

    .subscribe((getUserResponse) => {

      res.send(getUserResponse)

    })

})

router.delete('/', (req, res) => {

  return user

    .delete(req.query)

    .subscribe((getUserResponse) => {

      res.send(getUserResponse)

    })

})

var userFromBody = function(user) {

  return user;

}

module.exports = router;