import { CALL_API, Schemas } from '../middleware/api'

export const DATA_REQUEST = 'DATA_REQUEST'
export const DATA_SUCCESS = 'DATA_SUCCESS'
export const DATA_FAILURE = 'DATA_FAILURE'

const fetchData = (path, body) => ({
  [CALL_API]: {
    types: [DATA_REQUEST, DATA_SUCCESS, DATA_FAILURE],
    endpoint: `/${path}`,
    body
  }
})

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export const loadData = (path) => (dispatch, getState) => {

  return dispatch(fetchData(path))
}



var createRequestPackage = function(state) {

  return {
    "document": {
      "type": "PLAIN_TEXT",
      "language": "English",
      "content": state.data,
    //  "gcsContentUri": "string",
    },
    "encodingType": "UTF8",
  }

}
