//Actions
export const SHOW_DIALOG = 'SHOW DIALOG'
export const CLOSE_DIALOG = 'CLOSE DIALOG'

const dialogActionCreator = (config, type) => ({
  type,

  [type]: {
    ...config
  }
})

export const showDialog = (config) => (dispatch, getState) => {

  return dispatch(dialogActionCreator(config, SHOW_DIALOG))
}



export const closeDialog = (config) => (dispatch, getState) => {

  return dispatch(dialogActionCreator(config, CLOSE_DIALOG))
}