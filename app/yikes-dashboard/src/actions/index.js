export * from './login'

export * from './dialogs'

export * from './userSites'

export * from './siteData'

export * from './sitePages'

export * from './pageData'

export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE'

// Resets the currently visible error message.
export const resetErrorMessage = () => ({
  type: RESET_ERROR_MESSAGE
})



