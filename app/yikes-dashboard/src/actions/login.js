import { CALL_API, Schemas } from '../middleware/api'

export const USER_DATA_REQUEST = 'USER_DATA_REQUEST'
export const USER_DATA_SUCCESS = 'USER_DATA_SUCCESS'
export const USER_DATA_FAILURE = 'USER_DATA_FAILURE'

export const LOG_OUT = 'LOG_OUT'

const loginPath = "/users"

const loginActionCreator = (body) => ({
  [CALL_API]: {
    types: [USER_DATA_REQUEST, USER_DATA_SUCCESS, USER_DATA_FAILURE],
    endpoint: `${loginPath}`,
    config: {

      method: "POST",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(body)

    }
  }
})

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export const loginDispachConnector = (userInfo) => (dispatch, getState) => {

  return dispatch(loginActionCreator(userInfo))
}

const LocalLogOutAction = () => {

  return {
    type: LOG_OUT
  }

}


const logoutActionCreator = (body) => ({
  [CALL_API]: {
    types: [USER_DATA_REQUEST, USER_DATA_SUCCESS, USER_DATA_FAILURE],
    endpoint: `${loginPath}`,
    config: {

      method: "POST",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(body)

    }
  }
})

// Fetches a single user from Github API unless it is cached.
// Relies on Redux Thunk middleware.
export const logout = () => (dispatch, getState) => {

  return dispatch(LocalLogOutAction())
}



