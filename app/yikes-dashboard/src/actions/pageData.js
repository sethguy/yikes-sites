import { CALL_API } from '../middleware/api'

export const PAGE_DATA_REQUEST = 'PAGE_DATA_REQUEST'
export const PAGE_DATA_SUCCESS = 'PAGE_DATA_SUCCESS'
export const PAGE_DATA_FAILURE = 'PAGE_DATA_FAILURE'

const pageDataPath = "/site-pages"

export const fetchPageData = (pageInfo) => ({
  [CALL_API]: {
    types: [PAGE_DATA_REQUEST, PAGE_DATA_SUCCESS, PAGE_DATA_FAILURE],
    endpoint: `${pageDataPath}?_id=${pageInfo._id}`,
    config: {

      method: "Get",

    }
  }
})

export const loadPageData = (pageInfo) => (dispatch, getState) => {

  return dispatch(fetchPageData(pageInfo))
}

export const CLEAR_PAGE_DATA = 'CLEAR_PAGE_DATA'

const clearPageDataAction = (page) => ({
  type: CLEAR_PAGE_DATA
})

export const clearPageData = (page) => (dispatch, getState) => {

  return dispatch(clearPageDataAction(page))
}


export const PATCH_PAGE_DATA = 'PATCH_PAGE_DATA'

const patchPageDataAction = (pageData) => ({
  type: PATCH_PAGE_DATA,
  payload: pageData
})

export const patchPageData = (pageData) => (dispatch, getState) => {

  return dispatch(patchPageDataAction(pageData))
}

export const SAVE_PAGE_REQUEST = 'SAVE_PAGE_REQUEST'
export const SAVE_PAGE_SUCCESS = 'SAVE_PAGE_SUCCESS'
export const SAVE_PAGE_FAILURE = 'SAVE_PAGE_FAILURE'

const savePageAction = (pageData) => ({
  [CALL_API]: {
    types: [SAVE_PAGE_REQUEST, SAVE_PAGE_SUCCESS, SAVE_PAGE_FAILURE],
    endpoint: `${pageDataPath}`,
    config: {

      method: pageData._id ? "PUT" : "POST",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(pageData)

    }
  }
})

export const savePage = (pageData) => (dispatch, getState) => {

  return dispatch(savePageAction(pageData))
}

export const DELETE_PAGE_REQUEST = 'DELETE_PAGE_REQUEST'
export const DELETE_PAGE_SUCCESS = 'DELETE_PAGE_SUCCESS'
export const DELETE_PAGE_FAILURE = 'DELETE_PAGE_FAILURE'

const deletePageAction = (pageData) => ({
  [CALL_API]: {
    types: [DELETE_PAGE_REQUEST, DELETE_PAGE_SUCCESS, DELETE_PAGE_FAILURE],
    endpoint: `${pageDataPath}`,
    config: {

      method: "DELETE",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(pageData)

    }
  }
})

export const deletePage = (pageData) => (dispatch, getState) => {

  return dispatch(deletePageAction(pageData))
}
