import { CALL_API } from '../middleware/api'

export const SITE_DATA_REQUEST = 'SITE_DATA_REQUEST'
export const SITE_DATA_SUCCESS = 'SITE_DATA_SUCCESS'
export const SITE_DATA_FAILURE = 'SITE_DATA_FAILURE'

const siteDataPath = "/site-data"

const fetchSiteData = (siteData) => ({
  [CALL_API]: {
    types: [SITE_DATA_REQUEST, SITE_DATA_SUCCESS, SITE_DATA_FAILURE],
    endpoint: `${siteDataPath}?_id=${siteData._id}`,
    config: {
      method: "GET",
    }
  }
})

export const loadSiteData = (siteData) => (dispatch, getState) => {

  return dispatch(fetchSiteData(siteData))
}

export const CLEAR_SITE_DATA = 'CLEAR_SITE_DATA'

const clearSiteDataAction = (siteData) => ({
  type: CLEAR_SITE_DATA
})

export const clearSiteData = (siteData) => (dispatch, getState) => {

  return dispatch(clearSiteDataAction(siteData))
}

export const PATCH_SITE_DATA = 'PATCH_SITE_DATA'

const patchSiteDataAction = (siteData) => ({
  type: PATCH_SITE_DATA,
  payload: siteData
})

export const patchSiteData = (siteData) => (dispatch, getState) => {

  return dispatch(patchSiteDataAction(siteData))
}

export const SAVE_SITE_REQUEST = 'SAVE_SITE_REQUEST'
export const SAVE_SITE_SUCCESS = 'SAVE_SITE_SUCCESS'
export const SAVE_SITE_FAILURE = 'SAVE_SITE_FAILURE'

const saveSiteAction = (siteData) => ({
  [CALL_API]: {
    types: [SAVE_SITE_REQUEST, SAVE_SITE_SUCCESS, SAVE_SITE_FAILURE],
    endpoint: `${siteDataPath}`,
    config: {

      method: siteData._id ? "PUT" : "POST",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(siteData)

    }
  }
})

export const saveSite = (siteData) => (dispatch, getState) => {

  return dispatch(saveSiteAction(siteData))
}

export const DELETE_SITE_REQUEST = 'DELETE_SITE_REQUEST'
export const DELETE_SITE_SUCCESS = 'DELETE_SITE_SUCCESS'
export const DELETE_SITE_FAILURE = 'DELETE_SITE_FAILURE'

const deleteSiteAction = (siteData) => ({
  [CALL_API]: {
    types: [DELETE_SITE_REQUEST, DELETE_SITE_SUCCESS, DELETE_SITE_FAILURE],
    endpoint: `${siteDataPath}`,
    config: {

      method: "DELELTE",

      headers: {
        "Content-type": "application/json"
      },

      body: JSON.stringify(siteData)

    }
  }
})

export const deleteSite = (siteData) => (dispatch, getState) => {

  return dispatch(deleteSiteAction(siteData))
}