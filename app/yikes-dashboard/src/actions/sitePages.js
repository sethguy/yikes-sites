import { CALL_API } from '../middleware/api'

export const SITE_PAGES_REQUEST = 'SITE_PAGES_REQUEST'
export const SITE_PAGES_SUCCESS = 'SITE_PAGES_SUCCESS'
export const SITE_PAGES_FAILURE = 'SITE_PAGES_FAILURE'

const sitePagesPath = "/site-data/pages"

export const fetchSitePages = (siteInfo) => ({
  [CALL_API]: {
    types: [SITE_PAGES_REQUEST, SITE_PAGES_SUCCESS, SITE_PAGES_FAILURE],
    endpoint: `${sitePagesPath}/${siteInfo._id}`,
    config: {

      method: "Get",

    }
  }
})

export const loadSitePages = (siteInfo) => (dispatch, getState) => {

  return dispatch(fetchSitePages(siteInfo))
}

export const CLEAR_SITE_PAGES = 'CLEAR_SITE_PAGES'

const clearSitePagesAction = () => ({
  type: CLEAR_SITE_PAGES
})

export const clearSitePages = () => (dispatch, getState) => {

  return dispatch(clearSitePagesAction())
}