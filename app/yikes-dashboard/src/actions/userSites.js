import { CALL_API } from '../middleware/api'

export const USER_SITES_REQUEST = 'USER_SITES_REQUEST'
export const USER_SITES_SUCCESS = 'USER_SITES_SUCCESS'
export const USER_SITES_FAILURE = 'USER_SITES_FAILURE'

const userSitesPath = "/users/sites"

export const fetchUserSites = (userInfo) => ({
  [CALL_API]: {
    types: [USER_SITES_REQUEST, USER_SITES_SUCCESS, USER_SITES_FAILURE],
    endpoint: `${userSitesPath}/${userInfo._id}`,
    config: {

      method: "Get",

    }
  }
})

export const loadUserSites = (userInfo) => (dispatch, getState) => {

  return dispatch(fetchUserSites(userInfo))
}

export const CLEAR_USER_SITES = 'CLEAR_USER_SITES'

const clearUserSitesAction = () => ({
  type: CLEAR_USER_SITES
})

export const clearUserSites = () => (dispatch, getState) => {

  return dispatch(clearUserSitesAction())
}