import React, { Component } from 'react';
import { connect } from 'react-redux'
import { closeDialog } from '../../actions'
import Dialog from 'material-ui/Dialog';

class Dialogs extends Component {

  constructor(props) {
    super(props);

  }

  handleClose(name) {

    this.props.closeDialog({
      [name]: {
        open: false
      }
    })
  }

  render() {

    return (

      <div>
        { this.props.dialogs.basic &&
          
          <Dialog title="Dialog With Actions" modal={ false } open={ this.props.dialogs.basic.open } onRequestClose={ (event) => this.handleClose('basic') }>
            The actions in this window were passed in as an array of React objects.
          </Dialog> }
      </div>

      );
  }

}

const mapStateToProps = (state, ownProps) => ({
  dialogs: state.dialogs
})

export default connect(mapStateToProps, {
  closeDialog
})(Dialogs)
