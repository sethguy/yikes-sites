import React, { Component } from 'react';
import { connect } from 'react-redux'
import { loginDispachConnector } from '../actions'

import { FlexTable } from './shared'

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      LoginAlertMsg: "wrong password",
      alertVisibilty: 'hidden',
      alertLevel: 'warning'
    }
  }

  login = () => {

    var email = document.getElementById('userEmail').value

    var password = document.getElementById('userPassword').value

    var userInfo = {
      email,
      password
    }

    this.props.loginDispachConnector({
      login: {
        ...userInfo
      }
    })

  }

  closeAlert() {
    this.setState({
      alertVisibilty: 'hidden'
    })
  }

  showAlert(msg) {
    this.setState({
      LoginAlertMsg: msg.text,
      alertVisibilty: 'visible',
      alertLevel: msg.class
    })

  }

  render() {

    return (

      <div className="wholeView container">
        <div className="row">
          <div className="col-sm-4"></div>
          <div className="col-sm-4">
            <div className={ "alert alert-" + this.state.alertLevel } style={ { visibility: this.state.alertVisibilty } } role="alert">
              <button type="button" className="close" onClick={ () => {
                                                                  this.closeAlert()
                                                                } } aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Oh snap!</strong>
              <br/>
              { this.state.LoginAlertMsg }
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-4"></div>
          <div className="col-sm-4">
            <input placeholder='Email' id='userEmail' className="form-control input-lg text-center" />
            <br/>
            <input type='password' placeholder='Password' id='userPassword' className="form-control input-lg text-center" />
          </div>
        </div>
        <br/>
        <div className="row">
          <div className="col-sm-4"></div>
          <div className="col-sm-1"></div>
          <div className="col-sm-2 d-flex justify-content-center">
            <button className="btn btn-info" onClick={ () => {
                                                         this.login()
                                                       } }>
              Login
            </button>
          </div>
        </div>
      </div>

      );
  }

}

const mapStateToProps = (state, ownProps) => ({

})

export default connect(mapStateToProps, {
  loginDispachConnector
})(Login)
