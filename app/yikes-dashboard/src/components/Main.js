import React, { Component } from 'react';
import { connect } from 'react-redux'
import { showDialog, logout, loadUserSites } from '../actions'
import RaisedButton from 'material-ui/RaisedButton';

import { FlexTable, Title } from './shared'

var sitesTableRows = [
  {
    title: "Name",
    key: "name"
  },
  {
    title: "Url",
    key: "url"
  },

]

var list = [
  {
    name: "seth",
    pages: 3,
    url: "https://live.yikes.site/demo-page"
  }
]

const StartNewSiteButton = () => (
  <RaisedButton onClick={ (event) => window.location.hash = "manage-site" } label="Start New Site" />)


class Main extends Component {

  constructor(props) {
    super(props);
    this.state = {
      LoginAlertMsg: "wrong password",
      alertVisibilty: 'hidden',
      alertLevel: 'warning'
    }
  }

  componentWillMount() {

    var {userInfo: {_id}, loadUserSites} = this.props

    loadUserSites({
      _id
    })

  }

  logout() {

    this.props.logout()
  }

  showDialog() {

    this.props.showDialog({
      basic: {
        open: true
      }
    })
  }

  onSiteSelect(site) {

    window.location.hash = `manage-site/${site._id}`

  }

  render() {
    return (

      <div className="wholeView container">
        <br/>
        <div className="row">
          <div className="col-sm-12">
            <Title {...this.props} title={ "Dashboard" } ActionButtonRight={ StartNewSiteButton } />
          </div>
        </div>
        <hr/>
        <br/>
        <div className="row">
          <div className="col-sm-12">
            <h3>Sites</h3>
            <br/>
            { this.props.userSites && this.props.userSites.length > 0 &&
              <FlexTable selectItem={ this.onSiteSelect } items={ this.props.userSites } tableRows={ sitesTableRows } /> }
          </div>
        </div>
      </div>

      );
  }

}

const mapStateToProps = (state, ownProps) => ({
  userInfo: state.userInfo,
  userSites: Object.keys(state.userSites).map((siteDataKey) => state.userSites[siteDataKey])
})

export default connect(mapStateToProps, {
  showDialog,
  logout,
  loadUserSites
})(Main)
