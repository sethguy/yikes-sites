import React, { Component } from 'react';
import { connect } from 'react-redux'
import { loadPageData, clearPageData, savePage, patchPageData } from '../actions'
import RaisedButton from 'material-ui/RaisedButton';
//5aa6be6947e77b285063cd7d
import { FlexTable, Title } from './shared'

import SettingsMenu from './settings-menu'


const SavePageButton = ({pageData, savePage}) => (

  <RaisedButton onClick={ (event) => savePage(pageData) } label="Save" />)

const EditPageButton = (props) => (<SettingsMenu {...props} />)

const ManagePageForm = ({pageData, sitePages, savePage, patchPageData}) => (

  <div>
    <div className="row">
      <div className="col-sm-6">
        <input onChange={ (event) => {
                            patchPageData({
                              ...pageData,
                              name: event.target.value
                            })
                          } } value={ pageData.name } name="name" placeholder="Page name" className="page-input-form-feild form-control input-lg" />
      </div>
    </div>
    <hr/>
    <div className="row">
      <div className="col-sm-6">
        <SavePageButton savePage={ savePage } pageData={ pageData } />
      </div>
    </div>
  </div>

)

const NewPageForm = ({pageData, savePage, patchPageData}) => (

  <div>
    <div className="row">
      <div className="col-sm-6">
        <input onChange={ (event) => {
                            patchPageData({
                              ...pageData,
                              name: event.target.value
                            })
                          } } value={ pageData.name } name="name" placeholder="Page name" className="page-input-form-feild form-control input-lg" />
      </div>
    </div>
    <br/>
    <hr/>
    <div className="row">
      <div className="col-sm-6">
        <SavePageButton savePage={ savePage } pageData={ pageData } />
      </div>
    </div>
  </div>

)

class ManagePage extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {

    var {pageData, clearPageData, loadPageData, routing: {locationBeforeTransitions: {hash}}} = this.props

    var [sign, path, _id] = hash.split('/');

    if (_id) {
      loadPageData({
        _id
      })

    } else if (pageData && pageData._id) {

      clearPageData(pageData);

    }
  }

  render() {

    var {pageData} = this.props;

    return (

      <div className="wholeView container">
        <br/>
        <div className="row">
          <div className="col-sm-12">
            <Title title={ pageData._id ? "Manage Page" : "Create Page" } {...this.props}>
              { pageData._id && <EditPageButton {...this.props} /> }
            </Title>
          </div>
        </div>
        <br/>
        { pageData._id ?
          <ManagePageForm {...this.props}/> :
          <NewPageForm {...this.props}/> }
        <br/>
      </div>

      );
  }

}

const mapStateToProps = (state, ownProps) => ({
  pageData: {
    ...state.pageData,
    siteId: state.pageData.siteId || state.siteData._id
  },
  routing: state.routing,
  userInfo: state.userInfo
})

export default connect(mapStateToProps, {
  loadPageData,
  clearPageData,
  savePage,
  patchPageData
})(ManagePage)
