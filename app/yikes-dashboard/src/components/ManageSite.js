import React, { Component } from 'react';
import { connect } from 'react-redux'
import { loadSiteData, loadSitePages, clearSiteData, clearSitePages, patchSiteData, saveSite, logout } from '../actions'
import RaisedButton from 'material-ui/RaisedButton';

import { FlexTable, Title } from './shared'

const SitePagesList = ({siteData, sitePages, onPageSelect}) => (
  <FlexTable items={ sitePages } selectItem={ onPageSelect } tableRows={ [{ key: "name", title: "Name" }] } />)

const SaveSiteButton = ({siteData, saveSite}) => (

  <RaisedButton onClick={ (event) => saveSite(siteData) } label="Save" />)

const ManageSiteForm = ({siteData, sitePages, onPageSelect, patchSiteData, saveSite}) => (

  <div>
    <div className="row">
      <div className="col-sm-6">
        <input onChange={ (event) => {
                            patchSiteData({
                              ...siteData,
                              name: event.target.value
                            })
                          } } value={ siteData.name } name="name" placeholder="Site name" className="site-input-form-feild form-control input-lg" />
      </div>
    </div>
    <br/>
    <hr/>
    <div className="row">
      <div className="col-sm-3">
        <h3>Pages</h3>
      </div>
      <div className="col-sm-3">
        <RaisedButton onClick={ (event) => window.location.hash = "manage-page" } label="Add Page" />
      </div>
    </div>
    <br/>
    <div className="row">
      <div className="col-sm-12">
        <SitePagesList onPageSelect={ onPageSelect } sitePages={ sitePages } />
      </div>
    </div>
    <hr/>
    <div className="row">
      <div className="col-sm-6">
        <SaveSiteButton saveSite={ saveSite } siteData={ siteData } />
      </div>
    </div>
  </div>

)

const NewSiteForm = ({siteData, saveSite, patchSiteData}) => (

  <div>
    <div className="row">
      <div className="col-sm-6">
        <input onChange={ (event) => {
                            patchSiteData({
                              ...siteData,
                              name: event.target.value
                            })
                          } } value={ siteData.name } name="name" placeholder="Site name" className="site-input-form-feild form-control input-lg" />
      </div>
    </div>
    <br/>
    <hr/>
    <div className="row">
      <div className="col-sm-6">
        <SaveSiteButton saveSite={ saveSite } siteData={ siteData } />
      </div>
    </div>
  </div>

)

class ManageSite extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {

    var {siteData, clearSiteData, loadSiteData, loadSitePages, routing: {locationBeforeTransitions: {hash}}} = this.props

    var [sign, path, _id] = hash.split('/');

    if (_id) {
      loadSiteData({
        _id
      })

      loadSitePages({
        _id
      })
    } else if (siteData && siteData._id) {

      clearSiteData(siteData);

    }
  }

  onPageSelect(page) {

    window.location.hash = `manage-page/${page._id}`

  }

  render() {

    var {siteData, sitePages} = this.props;

    return (

      <div className="wholeView container">
        <br/>
        <Title title={ siteData._id ? "Manage Site" : "Create Site" } {...this.props}/>
        <br/>
        { siteData._id ?
          <ManageSiteForm onPageSelect={ this.onPageSelect } {...this.props}/> :
          <NewSiteForm {...this.props}/> }
        <br/>
      </div>

      );
  }

}

const mapStateToProps = (state, ownProps) => ({
  siteData: {
    ...state.siteData,
    user: state.siteData.user || state.userInfo._id
  },
  sitePages: Object.keys(state.sitePages).map((Key) => state.sitePages[Key]),
  routing: state.routing
})

export default connect(mapStateToProps, {
  loadSiteData,
  loadSitePages,
  clearSiteData,
  logout,
  saveSite,
  clearSitePages,
  patchSiteData
})(ManageSite)
