import React, { Component } from 'react';

import SettingsIcon from 'material-ui/svg-icons/action/settings';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';

var settingsMenu = ({pageData, userInfo}) => (
  <IconMenu className="settings-button" iconButtonElement={ <IconButton>
                                                            <SettingsIcon />
                                                          </IconButton> } anchorOrigin={ { horizontal: 'left', vertical: 'top' } } targetOrigin={ { horizontal: 'left', vertical: 'top' } }>
    <MenuItem onClick={ (event) => window.location.href = `/editor/${pageData._id}?access-token=${userInfo.accessToken}` } primaryText="Edit" />
    <MenuItem onClick={ (event) => window.location.href = `/live/${pageData._id}` } primaryText="see live" />
  </IconMenu>

)

export default settingsMenu