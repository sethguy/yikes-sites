import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const Title = ({title, ActionButtonRight, logout, left, children}) => (
  <div className="row d-flex align-items-center">
    <div className="col-sm-4">
      <div className="d-flex justify-content-between">
        <RaisedButton onClick={ (event) => logout() } label="LogOut" />
        { ActionButtonRight && <ActionButtonRight/> }
        { children }
      </div>
    </div>
    <div className="col-sm-4">
      <div className="d-flex justify-content-center">
        <h1>{ title }</h1>
      </div>
    </div>
  </div>)


export default Title;