import React, { Component } from 'react'
import PropTypes from 'prop-types'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { connect } from 'react-redux'

import createHistory from 'history/createBrowserHistory'

import { browserHistory } from 'react-router'
import { HashRouter } from 'react-router-dom'
import { Route } from 'react-router-dom'

import { resetErrorMessage } from '../actions'

import Main from '../components/Main';
import Login from '../components/Login'
import ManageSite from '../components/ManageSite'

import ManagePage from '../components/ManagePage'


import Dialogs from '../components/Dialogs'


import "../Styles/index.css"

const history = createHistory()

class App extends Component {
  static propTypes = {
    // Injected by React Redux
    errorMessage: PropTypes.string,
    resetErrorMessage: PropTypes.func.isRequired,
    // Injected by React Router
    children: PropTypes.node
  }

  handleDismissClick = e => {
    this.props.resetErrorMessage()
    e.preventDefault()
  }

  renderErrorMessage() {
    const {errorMessage} = this.props
    if (!errorMessage) {
      return null
    }

    return (
      <p style={ { backgroundColor: '#e99', padding: 10 } }>
        <b>{ errorMessage }</b>
        { ' ' }
        <button onClick={ this.handleDismissClick }>
          Dismiss
        </button>
      </p>
    )
  }

  render() {
    const {children, inputValue} = this.props
    return (

      <MuiThemeProvider>
        <HashRouter history={ history } basename='/'>
          { this.props.userInfo._id ?
            <div className="App">
              <Route exact path="/" component={ Main } />
              <Route exact path="/dashboard" component={ Main } />
              <Route exact path="/manage-site" component={ ManageSite } />
              <Route exact path="/manage-site/:id" component={ ManageSite } />
              <Route exact path="/manage-page" component={ ManagePage } />
              <Route exact path="/manage-page/:id" component={ ManagePage } />
              <Route exact path="/Login" component={ Main } />
              <div>
                <Dialogs/>
              </div>
            </div>
            :
            <div className="App">
              <Login/>
              <div>
                <Dialogs/>
              </div>
            </div> }
        </HashRouter>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  errorMessage: state.errorMessage,
  userInfo: state.userInfo
})

export default connect(mapStateToProps, {
  resetErrorMessage
})(App)
