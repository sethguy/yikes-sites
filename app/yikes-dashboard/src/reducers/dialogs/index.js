import merge from 'lodash/merge'
import * as ActionTypes from '../../actions'

const dialogs = (state = {

  }, action) => {

  if (action && action[ActionTypes.SHOW_DIALOG]) {
    return merge({}, state, action[ActionTypes.SHOW_DIALOG])
  }

  if (action && action[ActionTypes.CLOSE_DIALOG]) {
    return merge({}, state, action[ActionTypes.CLOSE_DIALOG])
  }

  return state

}

export default dialogs