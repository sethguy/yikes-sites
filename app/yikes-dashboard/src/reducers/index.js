import * as ActionTypes from '../actions'
import merge from 'lodash/merge'

import userInfo from './userInfo'
import userSites from './userSites'

import siteData from './siteData'

import sitePages from './sitePages'

import pageData from './pageData'

import dialogs from './dialogs'

import { routerReducer as routing } from 'react-router-redux'
import { combineReducers } from 'redux'

// Updates error message to notify about the failed fetches.
const errorMessage = (state = null, action) => {
  const {type, error} = action

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null
  } else if (error) {
    return error
  }

  return state
}

// Updates the pagination data for different actions.


const rootReducer = combineReducers({
  pageData,
  userSites,
  sitePages,
  userInfo,
  dialogs,
  siteData,
  errorMessage,
  routing
})

export default rootReducer
