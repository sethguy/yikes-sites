import merge from 'lodash/merge'
import * as ActionTypes from '../actions'

const pageData = (state = {

  }, action) => {

  if (action && action.type == ActionTypes.PAGE_DATA_SUCCESS && action.response) {

    localStorage.setItem("pageData", JSON.stringify(action.response))

  }

  if (action && action.type == ActionTypes.CLEAR_PAGE_DATA) {

    localStorage.setItem("pageData", "{}")

    return {}

  }
  if (action.type == ActionTypes.PATCH_PAGE_DATA) {

    localStorage.setItem("pageData", JSON.stringify(merge({}, state, action.payload)))

  }

  var pageData = localStorage.getItem("pageData")

  var parsedSite = JSON.parse(pageData)

  if (parsedSite) {

    return merge({}, state, parsedSite)

  }
  return state

}

export default pageData