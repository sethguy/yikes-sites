import merge from 'lodash/merge'
import * as ActionTypes from '../actions'

const siteData = (state = {

  }, action) => {

  if (action) {

    if (action.type == ActionTypes.SITE_DATA_SUCCESS && action.response && action.response.siteData) {

      localStorage.setItem("siteData", JSON.stringify(action.response.siteData))

    }

    if (action.type == ActionTypes.CLEAR_SITE_DATA) {

      localStorage.setItem("siteData", "{}")

      return {}

    }

    if (action.type == ActionTypes.PATCH_SITE_DATA) {

      localStorage.setItem("siteData", JSON.stringify(merge({}, state, action.payload)))

    }

  }

  var siteData = localStorage.getItem("siteData")

  var parsedSite = JSON.parse(siteData)

  if (parsedSite && (parsedSite._id || parsedSite.name || parsedSite.user)) {

    return merge({}, state, parsedSite)

  }

  return state

}

export default siteData