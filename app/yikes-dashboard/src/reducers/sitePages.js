import merge from 'lodash/merge'
import * as ActionTypes from '../actions'

const sitePages = (state = {

  }, action) => {

  if (action) {

    if (action.type == ActionTypes.SITE_PAGES_SUCCESS && action.response) {

      localStorage.setItem("sitePages", JSON.stringify(action.response))

    }

    if (action.type == ActionTypes.CLEAR_SITE_PAGES) {

      localStorage.setItem("sitePages", "[]")

      return []

    }

  }

  var sitePages = localStorage.getItem("sitePages")

  var parsedSitePages = JSON.parse(sitePages)

  if (parsedSitePages) {

    return parsedSitePages

  }

  return state

}

export default sitePages