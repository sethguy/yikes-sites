import merge from 'lodash/merge'
import * as ActionTypes from '../actions'

const userInfo = (state = {

  }, action) => {

  if (action
    && action.type == ActionTypes.USER_DATA_SUCCESS
    && action.response
    && action.response.userInfo
  ) {
    localStorage.setItem("userInfo", JSON.stringify(action.response.userInfo))
  }

  if (action
    && action.type == ActionTypes.LOG_OUT) {

    localStorage.setItem("userInfo", null)
    return {};
  }

  var userInfo = localStorage.getItem("userInfo")

  var parsedUserInfo = JSON.parse(userInfo)

  if (parsedUserInfo && parsedUserInfo._id) {


    return merge({}, state, parsedUserInfo)

  }

  return state

}

export default userInfo







