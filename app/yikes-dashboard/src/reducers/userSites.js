import merge from 'lodash/merge'
import * as ActionTypes from '../actions'

const userSites = (state = [], action) => {

  if (action && action.type == ActionTypes.USER_SITES_SUCCESS && action.response) {

    localStorage.setItem("userSites", JSON.stringify(action.response))

  }

  var userSites = localStorage.getItem("userSites")

  var parsedUserSites = JSON.parse(userSites)

  if (parsedUserSites) {

    return parsedUserSites

  }

  return state

}

export default userSites